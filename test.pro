TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_CXXFLAGS += -std=c++11

SOURCES += sort.cpp \
    main.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    sort.h

