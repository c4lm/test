#include<sort.h>
#include<iostream>
#include<vector>
#include<algorithm>

namespace wheels
{




template<class T>
void insertion_sort(std::vector<T>& v )
{
    unsigned int i,j;
    for (i = 1; i < v.size(); i++)
        {
            j = i;
            while (j > 0 and (v[j - 1] > v[j]))
            {
                std::swap(v[j],v[j-1]);
                j--;
            }
        }
}

void print_insertion_sort()
{
    std::vector<int> unsorted={-4,6,0,2,-10};

    for(auto i:unsorted)std::cout<<i<<" ";
    std::cout<<std::endl;

    insertion_sort(unsorted);
    //std::sort(unsorted.begin(), unsorted.end(), [](int x, int y){return x<y;});

    for(auto i:unsorted)std::cout<<i<<" ";
    std::cout<<std::endl;
}






}
